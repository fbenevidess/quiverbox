class CreateAdvertisements < ActiveRecord::Migration
  def change
    create_table :advertisements do |t|
      t.string :product, null: false, index: true
      t.text :description, null: false
      t.decimal :price, precision: 5, scale: 2

      t.boolean :featured, null: false, default: 0
      t.datetime :created_at, null: false
      t.datetime :expires_at, null: false

      t.string :brand, null: false
      t.references :user, null: false
      t.references :sport, null: false
      t.references :category, null: false
    end
  end
end
