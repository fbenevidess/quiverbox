class IncreasePrecisionAndScaleForPrice < ActiveRecord::Migration
  def change
    change_column :advertisements, :price, :decimal, precision: 10, scale: 2
  end
end
