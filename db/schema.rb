# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150416002336) do

  create_table "advertisements", force: :cascade do |t|
    t.string   "product",     limit: 255,                                            null: false
    t.text     "description", limit: 65535,                                          null: false
    t.decimal  "price",                     precision: 10, scale: 2
    t.boolean  "featured",    limit: 1,                              default: false, null: false
    t.datetime "created_at",                                                         null: false
    t.datetime "expires_at",                                                         null: false
    t.string   "brand",       limit: 255,                                            null: false
    t.integer  "user_id",     limit: 4,                                              null: false
    t.integer  "sport_id",    limit: 4,                                              null: false
    t.integer  "category_id", limit: 4,                                              null: false
  end

  add_index "advertisements", ["product"], name: "index_advertisements_on_product", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name",       limit: 255, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "sports", force: :cascade do |t|
    t.string "name", limit: 255, null: false
  end

  add_index "sports", ["name"], name: "index_sports_on_name", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",                   limit: 255,              null: false
    t.date     "birthday"
    t.string   "cpf",                    limit: 255
    t.string   "phone",                  limit: 255
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "unconfirmed_email",      limit: 255
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
