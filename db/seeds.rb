# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Category.create(
  [
    { name: 'Equipamento' },
    { name: 'Prancha' },
    { name: 'Camera' },
    { name: 'Lifestyle' }
  ])

Sport.create(
  [
    { name: 'Surfing' },
    { name: 'Bodyboarding' },
    { name: 'Skateboarding' },
    { name: 'Windsurfing' },
    { name: 'Kitesurfing' },
    { name: 'Longboarding' },
    { name: 'Escalada' },
    { name: 'Bodysurfing' },
    { name: 'BMX' },
    { name: 'Snowboarding' },
    { name: 'Sandboarding' },
    { name: 'Wakeboarding' },
    { name: 'Mergulho' },
    { name: 'Caiaque' }
  ])
