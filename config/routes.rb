Rails.application.routes.draw do
  devise_for :users, path: '/usuarios', path_names: {
    sign_in: 'entrar',
    sign_out: 'sair',
    sign_up: 'cadastro',
    password: 'senha',
    edit: 'editar',
    cancel: 'cancelar',
    confirmation: 'confirmacao'
  }, controllers: { registrations: 'auth/registrations' }

  namespace :my, path: '' do
    resources :advertisements, path: 'anuncios', except: [:show]
  end

  root to: 'home#index'
end
