describe Category, type: :model do
  let(:category) { FactoryGirl.build :category }
  subject { category }

  it { should respond_to(:name) }
end
