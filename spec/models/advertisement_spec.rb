describe Advertisement, type: :model do
  let(:ad) { FactoryGirl.build :advertisement }
  subject { ad }

  it { should respond_to(:product) }
  it { should respond_to(:description) }
  it { should respond_to(:brand) }
  it { should respond_to(:price) }
  it { should respond_to(:created_at) }
  it { should respond_to(:expires_at) }
  it { should respond_to(:user) }
  it { should respond_to(:sport) }
  it { should respond_to(:category) }

  it { should belong_to(:user) }
  it { should belong_to(:category) }
  it { should belong_to(:sport) }

  it { should validate_presence_of(:price) }
  it { should validate_presence_of(:product) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:brand) }

  it { should validate_numericality_of(:price).is_greater_than(0) }

  it { should be_valid }

  describe '.expired?' do
    context 'when created is out of the interval between created and finish dates' do
      let(:expired_ad) { FactoryGirl.build :expired_ad }

      it 'returns true' do
        expect(expired_ad).to be_expired
      end
    end

    context 'when today is between created and expires date' do
      it 'returns false' do
        expect(ad).not_to be_expired
      end
    end
  end
end
