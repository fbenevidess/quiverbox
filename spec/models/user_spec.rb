describe User, type: :model do
  let(:user) { FactoryGirl.build :user }
  subject { user }

  it { should respond_to(:name) }
  it { should respond_to(:birthday) }
  it { should respond_to(:cpf) }
  it { should respond_to(:phone) }
  it { should respond_to(:email) }

  it { should allow_value('email@example.com').for(:email) }
  it { should_not allow_value('xsssdsd.com').for(:email) }
  it { should validate_uniqueness_of(:email) }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:email) }

  it { should be_valid }
end
