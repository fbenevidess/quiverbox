describe Sport, type: :model do
  let(:sport) { FactoryGirl.build :sport }
  subject { sport }

  it { should respond_to(:name) }
end
