module Devise
  module AuthenticationHelper
    def user_signed_in
      user = FactoryGirl.create :confirmed_user
      allow(request.env['warden']).to receive(:authenticate!) { user }
      allow(controller).to receive(:current_user) { user }

      user
    end
  end
end
