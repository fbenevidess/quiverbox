describe My::AdvertisementsController, type: :controller do
  before do
    @current_user = user_signed_in

    FactoryGirl.create_list(:category, 5)
    FactoryGirl.create_list(:sport, 5)
  end

  describe '.new' do
    it 'renders a new advertisement' do
      get :new
      expect(assigns(:ad)).not_to be_blank
    end
  end

  describe '.index' do
    before do
      FactoryGirl.create_list(:advertisement, 10)
      FactoryGirl.create(:advertisement, user_id: @current_user.id)
    end

    it 'renders only ads for the current user' do
      get :index
      expect(assigns(:ads).count).to eql 1
    end
  end

  describe '.create' do
    context 'when all data are filled' do
      let(:ad) do
        {
          advertisement: {
            product: 'Bodyboard Genesis',
            price: 'R$ 400,00',
            brand: 'Geneses',
            description: 'Lifes Good',
            category_id: Category.first.id,
            sport_id: Sport.first.id
          }
        }
      end

      before { post :create, ad }

      it 'creates a new ad' do
        expect(Advertisement.count).to eql 1
      end

      it 'saves an ad with price converted to decimal' do
        advertisement = Advertisement.first
        expect(advertisement.price).to eql BigDecimal.new("400.00")
      end

      it 'sends a saved message' do
        expect(flash[:saved]).to eql 'Anúncio criado com sucesso.'
      end

      it 'renders list of advertisements' do
        expect(response).to redirect_to(my_advertisements_path)
      end
    end

    context 'when some data is missing' do
      let(:ad) do
        {
          advertisement: {
            product: 'Bodyboard Genesis',
            price: 'R$ 400,00',
            brand: 'Geneses',
            category_id: Category.first.id,
            sport_id: Sport.first.id
          }
        }
      end

      before { post :create, ad }

      it 'renders a list of one error' do
        expect(assigns(:ad).errors).to have_key(:description)
      end

      it 'renders the new template' do
        expect(response).to render_template('new')
      end
    end
  end

  describe '.update' do
    before {
      first = FactoryGirl.create :advertisement
      @expires_at = first.expires_at
    }

    context 'when update price successfully' do
      let(:ad_to_update) { Advertisement.first }

      before do
        json = JSON.parse(ad_to_update.to_json)
        json['price'] = 'R$ 30,00'

        patch :update, id: ad_to_update.id, advertisement: json
      end

      it 'does not create a new one' do
        expect(Advertisement.count).to eql 1
      end

      it 'sends an updated message' do
        expect(flash[:success]).to eql 'Anúncio atualizado com sucesso'
      end

      it 'should not update expires date' do
        expect(@expires_at.to_s).to eql assigns(:ad).expires_at.to_s
      end

      it 'updates only price' do
        expect(assigns(:ad).price).to eql BigDecimal.new('30.00')
      end
    end
  end
end
