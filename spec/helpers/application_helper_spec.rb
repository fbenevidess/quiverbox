describe ApplicationHelper, type: :helper do
  describe '#title' do
    it 'puts title wherever :title tag is' do
      title = 'Quiverbox rocks'

      helper.title(title)
      expect(helper.content_for(:title)).to eql title
    end
  end

  describe '#import_font_family' do
    it 'renders any font from href' do
      link = helper.import_font_family('Montserrat:400,700')
      expect(link).to eql "<link href=\"http://fonts.googleapis.com/css?family=Montserrat:400,700\" rel=\"stylesheet\" type=\"text/css\"></link>"
    end
  end
end