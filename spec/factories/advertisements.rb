FactoryGirl.define do
  factory :advertisement do
    product FFaker::Product.product_name
    description FFaker::Lorem.words(50).join(' ')
    price 199.99
    brand FFaker::Product.brand

    created_at DateTime.now
    expires_at DateTime.now + 30.days

    user
    category
    sport

    trait :expired do
      created_at 30.days.from_now
      expires_at 1.day.ago
    end

    trait :new do
      created_at nil
      expires_at nil
    end

    factory :expired_ad, traits: [:expired]
    factory :new, traits: [:new]
  end
end
