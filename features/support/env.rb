require 'cucumber/rails'
require 'capybara/cucumber'

ActionController::Base.allow_rescue = false

begin
  DatabaseCleaner.strategy = :truncation
rescue NameError
  raise "You need to add database_cleaner to your Gemfile (in the :test group) if you wish to use it."
end

Capybara.register_driver :webkit do |app|
  Capybara::Webkit::Driver.new(app).tap do |driver|
    driver.allow_url 'fonts.googleapis.com'
    driver.block_unknown_urls
  end
end

Capybara.default_driver = :webkit
Cucumber::Rails::Database.javascript_strategy = :truncation

Before do
  DatabaseCleaner.start
  page.driver.allow_url('fonts.googleapis.com')
end

After do |scenario|
  DatabaseCleaner.clean
end
