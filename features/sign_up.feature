Feature:
  As a guest,
  I want to join in Quiverbox
  So I can buy or sell everything related to xtreme sports

Scenario: When the guest fill everything ok
  Given I'm on sign up page
   When I fill "John Doe" in "user_name" field
    And I fill "john.doe@email.com" in "user_email" field
    And I fill "my precious" in "user_password" field
    And I fill "my precious" in "user_password_confirmation" field
    And I click on "Entrar agora" button
   Then I should be redirected to home
    And I should see a message modal
    And I should see an email confirmation message

Scenario: When e-mail is already in use
  Given I'm on sign up page
    And Quiverbox has an user with "john.doe@email.com" e-mail
   When I fill "John Doe" in "user_name" field
    And I fill "john.doe@email.com" in "user_email" field
    And I fill "my precious" in "user_password" field
    And I fill "my precious" in "user_password_confirmation" field
    And I click on "Entrar agora" button
   Then I see a warning box with "E-mail já está sendo usado." message

Scenario: When user forget to fill name field
  Given I'm on sign up page
   When I fill "john.doe@email.com" in "user_email" field
    And I fill "my precious" in "user_password" field
    And I fill "my precious" in "user_password_confirmation" field
    And I click on "Entrar agora" button
   Then I see a warning box with "Nome é um campo obrigatório" message

Scenario: When user forget to fill email field
  Given I'm on sign up page
   When I fill "John Doe" in "user_name" field
    And I fill "my precious" in "user_password" field
    And I fill "my precious" in "user_password_confirmation" field
    And I click on "Entrar agora" button
   Then I see a warning box with "E-mail é um campo obrigatório" message

Scenario: When user forget to fill password field
 Given I'm on sign up page
  When I fill "John Doe" in "user_name" field
   And I fill "my precious" in "user_password_confirmation" field
   And I click on "Entrar agora" button
  Then I see a warning box with "Senha é um campo obrigatório" message

Scenario: When password is too short
  Given I'm on sign up page
   When I fill "John Doe" in "user_name" field
    And I fill "12345" in "user_password" field
    And I fill "12345" in "user_password_confirmation" field
    And I click on "Entrar agora" button
   Then I see a warning box with "Senha precisa de no mínimo 8 caracteres" message
