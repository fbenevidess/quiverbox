When(/^I click on "(.*?)" dropdown menu$/) do |link|
  within '.menu-top' do
    click_link link
  end
end

Then(/^I see home screen with my name in header$/) do
  expect(current_path).to eql root_path
  expect(find('.navbar-right')).to have_content User.first.name
end

Then(/^I should see "(.*?)" in menu$/) do |menu|
  expect(find('.menu-top')).to have_content menu
end
