Given(/^I'm on sign up page$/) do
  visit new_user_registration_path
end

Then(/^I should see a message modal$/) do
  expect(find('#modal-message')).to be_visible
end

Then(/^I should see an email confirmation message$/) do
  expect(find('#modal-message')).to have_content 'Uma mensagem com um link de confirmação foi enviada para o seu e-mail'
end
