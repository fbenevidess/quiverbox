Given(/^There's a confirmed user in Quiverbox$/) do
  FactoryGirl.create(:confirmed_user, name: 'John Doe', email: 'user@example.com', password: 'teste123', password_confirmation: 'teste123')
end

Given(/^I'm on login page$/) do
  visit new_user_session_path
end

Given(/^I'm on home page$/) do
  visit root_path
end

Given(/^Quiverbox has an user with "(.*?)" e\-mail$/) do |email|
  FactoryGirl.create(:confirmed_user, email: email)
end

Given(/^I'm a logged user$/) do
  password = '12345678'
  user = FactoryGirl.create(:confirmed_user, password: password, password_confirmation: password)
  steps %{
    Given I'm on login page
    When I fill "#{user.email}" in "user_email" field
     And I fill "#{password}" in "user_password" field
     And I click on "Entrar" button
  }
end

When(/^I click on "(.*?)"$/) do |selector|
  click_on selector
end

When(/^I click on "(.*?)" link$/) do |link|
  click_link link
  sleep 2
end

When(/^I select "(.*?)" in "(.*?)" list$/) do |element, select|
  select(element, from: select)
end

When(/^I fill "(.*?)" in "(.*?)" field$/) do |value, field|
  fill_in field, with: value
end

When(/^I click on "(.*?)" button$/) do |button|
  click_button button
end

Then(/^I see a warning box with "(.*?)" message$/) do |message|
  expect(find('.alert-warning')).to have_content message
end

Then(/^I see a success box with "(.*?)" message$/) do |message|
  expect(find('.alert-success')).to have_content message
end

Then(/^I see an info box with "(.*?)" message$/) do |message|
  expect(find('.alert-info')).to have_content message
end

Then(/^I should be redirected to home$/) do
  expect(current_path).to eql root_path
end

Then(/^I should be redirected to login page$/) do
  expect(current_path).to eql new_user_session_path
end

Then(/^I see "(.*?)" in "(.*?)" field$/) do |value, field|
  expect(find_field(field).value).to eql value
end

Then(/^I should see "(.*?)" in page$/) do |value|
  expect(page).to have_content value
end
