Given(/^I'm on new advertisement page$/) do
  FactoryGirl.create(:category, name: 'Equipamento')
  FactoryGirl.create(:sport, name: 'Bodyboarding')

  visit new_my_advertisement_path
end
