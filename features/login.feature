Feature:
  As a guest
  I want to be able to enter my credentials
  So I can enter in Quiverbox

Scenario: When the credentials are correct
  Given I'm on login page
    And There's a confirmed user in Quiverbox
   When I fill "user@example.com" in "user_email" field
    And I fill "teste123" in "user_password" field
    And I click on "Entrar" button
   Then I see home screen with my name in header


Scenario: When the credentials are wrong
  Given I'm on login page
   When I fill "blabla@example.com" in "user_email" field
    And I fill "teste123" in "user_password" field
    And I click on "Entrar" button
   Then I see a warning box with "E-mail ou senha inválidos." message

Scenario: When user wants to log out of the Quiverbox
  Given I'm a logged user
    And I'm on home page
   When I click on "logged-user"
    And I click on "Sair" dropdown menu
   Then I should be redirected to home
    And I should see "Entrar" in menu
    And I should see "Cadastre-se" in menu
