Feature:
  As a user,
  I forget my password and I want to set a new one
  So I can enter in Quiverbox again

Scenario: When reset password normally
  Given I'm on login page
    And Quiverbox has an user with "john.doe@email.com" e-mail
   When I click on "Esqueci minha senha" link
    And I fill "john.doe@email.com" in "user_email" field
    And I click on "Enviar" button
   Then I should be redirected to login page
    And I see a warning box with "Dentro de minutos, você receberá um e-mail com instruções para a troca da sua senha." message

Scenario: Email not filled
  Given I'm on login page
   When I click on "Esqueci minha senha" link
    And I click on "Enviar" button
   Then I see a warning box with "E-mail é um campo obrigatório" message

Scenario: Email does not exists
  Given I'm on login page
   When I click on "Esqueci minha senha" link
    And I fill "john.doe@email.com" in "user_email" field
    And I click on "Enviar" button
   Then I see a warning box with "E-mail não encontrado" message
