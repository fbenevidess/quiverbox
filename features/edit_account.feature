Feature:
  As a logged user,
  I want to edit my account
  So I can create an AD with my correct contacts

Scenario: When the user edits name and e-mail
  Given I'm a logged user
    And I'm on edit account page
   When I fill "John Doe" in "user_name" field
    And I fill "john.doe@email.com" in "user_email" field
    And I fill "12345678" in "user_current_password" field
    And I click on "Atualizar Informações" button
   Then I see a success box with "Você atualizou a sua conta com sucesso, mas o seu novo endereço de e-mail" message
    And I see an info box with "Aguardando confirmação do e-mail: john.doe@email.com" message
    And I see "John Doe" in "user_name" field

Scenario: When the user forget to fill his current password
  Given I'm a logged user
    And I'm on edit account page
   When I fill "John Doe" in "user_name" field
    And I fill "john.doe@email.com" in "user_email" field
    And I click on "Atualizar Informações" button
   Then I see a warning box with "Senha atual é um campo obrigatório" message

Scenario: When the user tries to change name to an empty one
  Given I'm a logged user
    And I'm on edit account page
   When I fill "" in "user_name" field
    And I fill "john.doe@email.com" in "user_email" field
    And I fill "12345678" in "user_current_password" field
    And I click on "Atualizar Informações" button
   Then I see a warning box with "Nome é um campo obrigatório" message

Scenario: When the user tries to change email to an empty one
 Given I'm a logged user
   And I'm on edit account page
  When I fill "John Doe" in "user_name" field
   And I fill "" in "user_email" field
   And I fill "12345678" in "user_current_password" field
   And I click on "Atualizar Informações" button
  Then I see a warning box with "E-mail é um campo obrigatório" message

Scenario: When the user put an invalid current password
  Given I'm a logged user
    And I'm on edit account page
   When I fill "John Doe" in "user_name" field
    And I fill "john.doe@email.com" in "user_email" field
    And I fill "MIMIMI" in "user_current_password" field
    And I click on "Atualizar Informações" button
   Then I see a warning box with "Senha atual está inválida." message
