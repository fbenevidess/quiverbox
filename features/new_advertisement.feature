Feature:
  As a logged user,
  I want to create a new AD
  So the people can see and buy it

Scenario: When the user creates a new ad successfully
  Given I'm a logged user
    And I'm on new advertisement page
   When I fill "Bodyboard GT Boards" in "advertisement_product" field
    And I fill "R$ 400,00" in "advertisement_price" field
    And I fill "GT Boards" in "advertisement_brand" field
    And I select "Equipamento" in "advertisement_category_id" list
    And I select "Bodyboarding" in "advertisement_sport_id" list
    And I fill "Usado mas em bom estado" in "advertisement_description" field
    And I click on "Criar anúncio" button
   Then I should see "Bodyboard GT Boards" in page

Scenario: When the user forgets to fill the product
 Given I'm a logged user
   And I'm on new advertisement page
  When I fill "R$ 400,00" in "advertisement_price" field
   And I fill "GT Boards" in "advertisement_brand" field
   And I select "Equipamento" in "advertisement_category_id" list
   And I select "Bodyboarding" in "advertisement_sport_id" list
   And I fill "Usado mas em bom estado" in "advertisement_description" field
   And I click on "Criar anúncio" button
  Then I see a warning box with "Produto é um campo obrigatório" message

Scenario: When the user forgets to fill the price
 Given I'm a logged user
   And I'm on new advertisement page
  When I fill "Bodyboard GT Boards" in "advertisement_product" field
   And I fill "GT Boards" in "advertisement_brand" field
   And I select "Equipamento" in "advertisement_category_id" list
   And I select "Bodyboarding" in "advertisement_sport_id" list
   And I fill "Usado mas em bom estado" in "advertisement_description" field
   And I click on "Criar anúncio" button
  Then I see a warning box with "Preço tem que ter um valor válido" message

  Scenario: When the user forgets to fill the brand
   Given I'm a logged user
     And I'm on new advertisement page
    When I fill "Bodyboard GT Boards" in "advertisement_product" field
     And I fill "R$ 400,00" in "advertisement_price" field
     And I select "Equipamento" in "advertisement_category_id" list
     And I select "Bodyboarding" in "advertisement_sport_id" list
     And I fill "Usado mas em bom estado" in "advertisement_description" field
     And I click on "Criar anúncio" button
    Then I see a warning box with "Marca é um campo obrigatório" message
