// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).on('page:change', function() {
  var MAX_COUNT = parseInt($('#advertisement_description').attr('maxlength'));

  $('#ad-description-md').on('keyup', function() {
    $('#chars-counter').text(MAX_COUNT - description.length)
  });

  $('#btn-preview').click(function(event) {
    event.preventDefault();
    var description = $('#advertisement_description').val();

    $('#advertisement_description').toggle();
    $('#md-preview').html(marked(description));
    $('#md-preview').toggle();
  });
});
