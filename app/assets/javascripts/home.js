$(document).ready(function() {
  $('#modal-message').modal('show')

  $(window).scroll(function() {
    var body = $(this);
    if (body.scrollTop() >= 70) {
      $('#navbar-fixed').show();
      $('#navbar-fixed').removeClass('slideInUp animated')
      $('#navbar-fixed').addClass('slideInDown animated')
    } else {
      $('#navbar-fixed').removeClass('slideInDown animated')
      $('#navbar-fixed').addClass('slideInUp animated')
      $('#navbar-fixed').hide();
    }
  });
});
