class User < ActiveRecord::Base
  validates :name, presence: true
  devise :database_authenticatable, :registerable, :confirmable, :recoverable, :rememberable, :validatable
end
