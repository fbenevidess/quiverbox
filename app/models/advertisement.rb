class Advertisement < ActiveRecord::Base
  scope :from_user, -> (user) { where user: user if user.present? }
  scope :most_featured, -> { where(featured: true).sample(12) }

  validates :product, presence: true
  validates :brand, presence: true
  validates :price, presence: true
  validates :description, presence: true

  validates_numericality_of :price, greater_than: 0

  belongs_to :user
  belongs_to :sport
  belongs_to :category

  before_create :set_period

  def expired?
    !DateTime.now.between?(created_at, expires_at)
  end

  private
  def set_period
    self.created_at = DateTime.now
    self.expires_at = self.created_at + 30.days
  end
end
