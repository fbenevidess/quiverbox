module ApplicationHelper
  def title(page_title)
    content_for :title, page_title.to_s
  end

  def import_font_family(font)
    content_tag :link, nil, href: "http://fonts.googleapis.com/css?family=#{font}", rel: 'stylesheet', type: 'text/css'
  end
end
