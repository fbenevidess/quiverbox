class HomeController < ApplicationController
  layout 'site'

  def index
    @featured = Advertisement.most_featured
  end
end
