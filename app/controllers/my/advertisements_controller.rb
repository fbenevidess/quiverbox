class My::AdvertisementsController < ApplicationController
  layout 'dashboard'
  before_action :authenticate_user!
  before_action :convert_price, only: [:create, :update]

  def new
    @ad = Advertisement.new
  end

  def create
    @ad = Advertisement.new(ad_params)
    @ad.price = @price
    @ad.user = current_user

    if @ad.save
      flash[:saved] = 'Anúncio criado com sucesso.'
      redirect_to my_advertisements_path
    else
      render :new
    end
  end

  def update
    @ad = Advertisement.find(params[:id])
    @ad.price = @price

    if @ad.update(ad_params)
      flash[:success] = 'Anúncio atualizado com sucesso'
      redirect_to my_advertisements_path
    end
  end

  def edit
    @ad = Advertisement.from_user(current_user).find(params[:id])
  end

  def index
    @ads = Advertisement.from_user(current_user)
  end

  private
  def ad_params
    params.require(:advertisement).permit(:product, :price, :brand, :description, :category_id, :sport_id)
  end

  def convert_price
    @price = Monetize.parse(ad_params[:price])
    params['advertisement'].delete('price')
  end
end
