VAGRANTFILE_API_VERSION = '2'

Vagrant.require_version '>= 1.5.0'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "ubuntu/trusty64"

  config.vm.provider :virtualbox do |vb|
    vb.customize ['modifyvm', :id, '--memory', '2048']
  end

  config.vm.synced_folder '.', '/quiver'

  config.vm.network 'forwarded_port', guest: 3000, host: 3002
  config.vm.network 'forwarded_port', guest: 3306, host: 3308

  config.vm.provision :chef_solo do |chef|
    chef.cookbooks_path = 'cookbooks'

    chef.add_recipe 'apt'
    chef.add_recipe 'build-essential'
    chef.add_recipe 'nodejs'

    # Ruby stuff
    chef.add_recipe 'ruby_build'
    chef.add_recipe "rbenv::user"
    chef.add_recipe "rbenv::vagrant"

    chef.add_recipe 'redisio'
    chef.add_recipe 'redisio::enable'

    chef.add_recipe 'imagemagick'

    # MySQL stuff for development
    chef.add_recipe 'mysql::server'
    chef.add_recipe 'mysql::client'

    chef.json = {
      rbenv: {
        user_installs: [{
          user: 'vagrant',
          rubies: ['2.1.2'],
          global: '2.1.2',
          gems: {
            '2.1.2' => [
              { name: 'bundler' }
            ]
          }
        }]
      },

      mysql: {
        server_root_user: 'root',
        server_root_password: ''
      }
    }
  end
end
